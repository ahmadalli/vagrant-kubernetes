#!/bin/bash
set -e

sudo curl -s https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash

cd /vagrant

helm upgrade --install simple-app charts/serve/ --atomic --namespace simple-app --create-namespace --wait --timeout=30m0s -f deploy/serve/values.yaml

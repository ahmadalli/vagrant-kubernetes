# Kubernetes Cluster with Vagrant

This repo is a demonstration of a production grade kubernetes cluster deployed using vagrant serving a simple API application deployed on the cluster using a helm chart.

## Design

### Application

ASP.Net Core is used for the simple api application. The project would be built on gitlab ci infrastructure and the resulting image would be used at the later stages.

### Kubernetes Cluster

The kubernetes cluster will be deployed using [kubespray](https://github.com/kubernetes-sigs/kubespray/) which is a collection of ansible playbooks used to setup and maintain a production ready kubernetes cluster. `Calico` is used as the CNI because of its performance and reliability. There are 3 etcd nodes (which must be an odd number in order to comply with quorum rule), two master nodes for master's high availability, and one worker. Normally there are more workers on the cluster but here only exists one because of the lack of resources on the host.

### Zero Downtime Deployment

The `deployment` resource of kubernetes **with `replicas: 2`** is used for deploying the application in order to achieve zero downtime deployment since `deployment` would upgrade pods one by one (by default, kubernetes ensures that at least 75% of the desired number of pods are up otherwise it'll upgrade pods one by one).

## Getting Started

### Prerequisites

Here's the list of tools you'll need to use this repo:

- clone this repo **and its submodules**
  ```bash
  git clone --recurse-submodules -j8 git@gitlab.com:ahmadalli/vagrant-kubernetes.git
  ```
- python 3
- pip 3
- vagrant >= 2.2
- (Optional, Recommended) [vagrant libvirt provider](https://github.com/vagrant-libvirt/vagrant-libvirt)
- (Required if you want to have http proxy support) vagrant plugin
  ```bash
  vagrant plugin install vagrant-proxyconf
  ```
- kubespray requirements (including ansible)
  ```bash
  pip install -r ansible/repos/kubespray/requirements.txt
  ```

### Running the Project

Vagrant's libvirt provider was used to run this repo. While other providers would also work, it's recommended to use libvirt provider since it has better performance on managing VMs. Since it supports parallel VM management, we first bring up the VMs, then provision them and setup the cluster and install the helm chart on the cluster

> if you need to use http proxy for internet connectivity, you need to set `http_proxy`, and `https_proxy` environmental variables
> ```bash
> export http_proxy=...
> export https_proxy=...
> ```

```bash
export VAGRANT_EXPERIMENTAL="dependency_provisioners"

vagrant up --no-provision
vagrant provision
```

after the provision has finised, the application (which is configured to have a nodePort on kubernetes service) is accessible on `localhost:31374/info/health` and `localhost:31374/info/version`

### Tearing Down the Project

run this command to tear down the VMs

```bash
vagrant destroy -f
```

### Troubleshooting

#### Ansible Failure

Some times ansible would fail due to network connectivity issues. You need to fix network connectivity, then provision the node which runs the ansible playbook again

```bash
vagrant provision k8s-master-2
```

#### Port Forwarding

Some times vagrant fails port forwarding and running `lsof -i :31374` returns no results. In that case you need to reload the node that does the port forwarding

```bash
vagrant reload k8s-master-2
```

using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace SimpleApi.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class InfoController : ControllerBase
  {
    [HttpGet("version")]
    public string Version()
    {
      return GetType().Assembly.GetName().Version.ToString();
    }

    [HttpGet("health")]
    public string Health()
    {
      return "healthy 😊";
    }
  }
}

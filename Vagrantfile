# -*- mode: ruby -*-
# # vi: set ft=ruby :

# this is based on kubespray's vagrant file

require 'fileutils'

Vagrant.require_version ">= 2.0.0"

vm_memory ||= 2048
vm_cpus ||= 2
subnet ||= "172.18.8"

num_etcd_instances = 3
num_kube_master_instances = 2
num_worker_instances = 1
instance_name_prefix = "k8s"
etcd_instance_name_prefix = "#{instance_name_prefix}-etcd"
kube_master_instance_name_prefix = "#{instance_name_prefix}-master"
worker_instance_name_prefix = "#{instance_name_prefix}-worker"
num_instances = num_worker_instances + num_etcd_instances + num_kube_master_instances

playbook ||= "ansible/repos/kubespray/cluster.yml"

host_vars = {}
(1..num_etcd_instances).each do |i|
  host_vars["%s-%01d" % [etcd_instance_name_prefix, i]] = {}
end
(1..num_worker_instances).each do |i|
  host_vars["%s-%01d" % [worker_instance_name_prefix, i]] = {}
end
(1..num_kube_master_instances).each do |i|
  host_vars["%s-%01d" % [kube_master_instance_name_prefix, i]] = {}
end

box = "generic/ubuntu1804"

if Vagrant.has_plugin?("vagrant-proxyconf")
    no_proxy = "127.0.0.1,localhost"
    (1..num_instances).each do |i|
        no_proxy += ",#{subnet}.#{i+100}"
    end
end

Vagrant.configure("2") do |config|
  config.vm.box = box

  # plugin conflict
  if Vagrant.has_plugin?("vagrant-vbguest") then
    config.vbguest.auto_update = false
  end

  # always use Vagrants insecure key
  config.ssh.insert_key = false

  host_vars.each_with_index  do |(hostname,dummy),index|
    config.vm.define vm_name = hostname do |node|
      node.vm.hostname = vm_name

      if Vagrant.has_plugin?("vagrant-proxyconf")
        node.proxy.http     = ENV['HTTP_PROXY'] || ENV['http_proxy'] || ""
        node.proxy.https    = ENV['HTTPS_PROXY'] || ENV['https_proxy'] ||  ""
        node.proxy.no_proxy = no_proxy
      end

      ["vmware_fusion", "vmware_workstation"].each do |vmware|
        node.vm.provider vmware do |v|
          v.vmx['memsize'] = vm_memory
          v.vmx['numvcpus'] = vm_cpus
        end
      end

      node.vm.provider :virtualbox do |vb|
        vb.memory = vm_memory
        vb.cpus = vm_cpus
        vb.linked_clone = true
        vb.customize ["modifyvm", :id, "--vram", "8"] # ubuntu defaults to 256 MB which is a waste of precious RAM
        vb.customize ["modifyvm", :id, "--audio", "none"]
      end

      node.vm.provider :libvirt do |lv|
        lv.cpu_mode = "host-model"
        lv.memory = vm_memory
        lv.cpus = vm_cpus
        lv.default_prefix = 'kubespray-'
      end

      node.vm.synced_folder ".", "/vagrant", disabled: false, type: "rsync", rsync__args: ['--verbose', '--archive', '--delete', '-z'] , rsync__exclude: ['.git','venv']

      ip = "#{subnet}.#{index + 1 + 100}"
      node.vm.network :private_network, ip: ip

      # Disable swap for each vm
      node.vm.provision "shell", inline: "swapoff -a"

      host_vars[vm_name] = {
        "ip": ip,
        "kube_network_plugin": "calico",
        "kubelet_cgroup_driver": "cgroupfs",
        "docker_cgroup_driver": "cgroupfs",
        "download_run_once": "True",
        "download_force_cache": "True",
        "download_localhost": "False",
        "download_cache_dir": ENV['HOME'] + "/kubespray_cache",
        "download_keep_remote_cache": "False",
        "docker_rpm_keepcache": "1",
        # These two settings will put kubectl and admin.config in $inventory/artifacts
        "kubeconfig_localhost": "True",
        "kubectl_localhost": "True",
        "ansible_ssh_user": "vagrant",
        "http_proxy": ENV['HTTP_PROXY'] || ENV['http_proxy'] || "",
        "https_proxy": ENV['HTTPS_PROXY'] || ENV['https_proxy'] || ""
      }

      # Only execute the Ansible provisioner once, when all the machines are up and ready.
      if index == num_instances - 1
        node.vm.provision "init cluster", type: "ansible" do |ansible|
          ansible.playbook = playbook
          ansible.become = true
          ansible.limit = "all,localhost"
          ansible.host_key_checking = false
          ansible.raw_arguments = ["--forks=#{num_instances}", "--flush-cache", "-e ansible_become_pass=vagrant"]
          ansible.host_vars = host_vars
          #ansible.tags = ['download']
          ansible.groups = {
            "etcd" => ["#{etcd_instance_name_prefix}-[1:#{num_etcd_instances}]"],
            "kube-master" => ["#{kube_master_instance_name_prefix}-[1:#{num_kube_master_instances}]"],
            "kube-node" => ["#{worker_instance_name_prefix}-[1:#{num_worker_instances}]"],
            "k8s-cluster:children" => ["kube-master", "kube-node"],
          }
        end

        node.vm.provision "install package", after:"init cluster", type: "shell", path: "deploy/install.sh"

        node.vm.network "forwarded_port", guest: 31374, host: 31374, protocol: "tcp"
      end
    end
  end
end
